import axios from 'axios';


export const getUser = async userId => {
    const {data} = await axios.get(`/api/users/profile/${userId}`)
    return data;
};

export const followUser = async followId => {
    const {data} = await axios.put ('/api/users/follow', {followId})
    return data;
}

export const unfollowUser = async followId => {
    const {data} = await axios.put ('/api/users/unfollow', {followId})
    return data;
}

export const deleteUser = async authUserid => {
    const {data} = await axios.delete (`/api/users/${authUserid}`)
    return data;
}

export const getAuthUser = async authUserid => {
    const {data} = await axios.get(`/api/users/${authUserid}`);
    return data;
}

export const updateUser = async (authUserid, userData) => {
    const {data} = await axios.put(`/api/users/${authUserid}`, userData);
    return data;
}

export const getUserFeed = async authUserid => {
    const {data} = await axios.get(`/api/users/feed/${authUserid}`);
    return data;
}

export const addPost = async (authUserid, post) => {
    const {data} = await axios.post(`/api/posts/new/${authUserid}`,post);
    return data;
}

export const getPostFeed = async authUserid => {
    const {data} = await axios.get(`/api/posts/feed/${authUserid}`);
    return data;
}

export const deletePost = async postId => {
    const {data} = await axios.delete(`/api/posts/${postId}`);
    return data;
}

export const likePost = async postId => {
    const {data} = await axios.put(`/api/posts/like/`, {postId});
    return data;
}

export const unlikePost = async postId => {
    const {data} = await axios.put(`/api/posts/unlike/`, {postId});
    return data;
}

export const addComment = async (postId, comment) => {
    const {data} = await axios.put(`/api/posts/comment`, {postId, comment});
    return data;
}

export const deleteComment = async (postId, comment) => {
    const {data} = await axios.put(`/api/posts/uncomment`, {postId, comment});
    return data;
}

export const getPostsByUser = async userId => {
    const {data} = await axios.get(`/api/posts/by/${userId}`);
    return data;
}